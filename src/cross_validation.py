# This script performs cross validation of the model.
# If run directly, it uses files with the "_cv.txt" subfix in the "data" folder.
# If included, it exports the cross_validation(model_name, max_epochs, train_tweets, train_labels, validation_tweets, validation_labels, test_tweets, test_labels) function..

iterations = 1	# Amount of times the cross validation shall be performed.
max_epochs = 120	# Maximum amount of epoches during training.
test_proportion = 0.1
stratified = True
multiply_classes = True

import numpy as np
from create_model import create_model
from test_model import test_model, common_metrics
from load import add_tweets, classes, binary, model_name, data_folder
import codecs
from sklearn.utils import shuffle

def cross_validation(structure_id, model_name, max_epochs, train_tweets, train_labels, validate_tweets, validate_labels, test_tweets, test_labels):
	""" Performs cross validation.
	@param  model_name      The name of the model (without extention) in the "models" folder.
	@param  max_epochs      The maximum amount of epoches during training.
	@param  train_tweets    The tweets to be used for training.
	@param  train_labels    The labels to be used for training.
	@param  validate_tweets The tweets to be used for early stopping.
	@param  validate_labels The labels to be used for early stopping.
	@param  test_tweets     The tweets to be used for testing.
	@param  test_labels     The labels to be used for testing.
	"""
	create_model(structure_id, model_name, False, max_epochs, train_tweets, train_labels, validate_tweets, validate_labels)
	return test_model(model_name, len(classes), binary, test_tweets, test_labels)


def get_cv_tweets():
	tweets = []
	max_length = 0
	for i in range(len(classes)):
		t = []
		fp = codecs.open("../data/" + data_folder + "/" + classes[i] + "_cv.txt", "r", "utf-8")
		ts = fp.readlines()
		add_tweets(ts, t)
		fp.close()
		tweets.append(t)
		if len(t) > max_length:
			max_length = len(t)
	return tweets


if __name__ == "__main__":
	tweets = get_cv_tweets()

	if multiply_classes:
		max_train_length = 0
		for i in range(len(classes)):
			if round(len(tweets[i]) * 0.8 * (1 - test_proportion)) > max_train_length:
				max_train_length = round(len(tweets[i]) * 0.8 * (1 - test_proportion))

	results = []
	metrics = []
	for i in range(iterations):
		train_tweets = []
		train_labels = []
		validate_tweets = []
		validate_labels = []
		test_tweets = []
		test_labels = []
		for i in range(len(classes)):
			tweets[i] = shuffle(tweets[i])
			train = tweets[i][:round(len(tweets[i]) * 0.8 * (1 - test_proportion))]
			if multiply_classes:
				train *= max_train_length // len(train)
			train_tweets += train
			train_labels += [i] * len(train)
			validate = tweets[i][round(len(tweets[i]) * 0.8 * (1 - test_proportion)) : round(len(tweets[i]) * (1 - test_proportion))]
			validate_tweets += validate
			validate_labels += [i] * len(validate)
			test = tweets[i][round(len(tweets[i]) * (1 - test_proportion)):]
			test_tweets += test
			test_labels += [i] * len(test)
		train_tweets, train_labels = shuffle(train_tweets, train_labels)
		validate_tweets, validate_labels = shuffle(validate_tweets, validate_labels)
		test_tweets, test_labels = shuffle(test_tweets, test_labels)

		result = cross_validation(0, model_name, max_epochs, train_tweets, train_labels, validate_tweets, validate_labels, test_tweets, test_labels)
		results.append(result)
		metrics.append(common_metrics(result))
	print()
	for i in range(iterations):
		print(results[i])
		print(metrics[i])
	result = np.sum(results, axis=0)
	metric = np.average(metrics, axis=0)
	print(result)
	print("Micro-averaged F1-score:", metric[0])
	print("Macro-averaged F1-score:", metric[1])
	print("Accuracy:", metric[2])
