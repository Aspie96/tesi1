# This script tests the current model.
# If called directly it tests the model on the current task using the files in the "data" folder with the "_test.txt" subfix.
# If included it exports the 

from keras.models import load_model
import numpy as np
from keras import backend as K
from load import add_tweets, classes, binary, model_name, data_folder
import codecs

def test_model(model_name, classes_count, binary, test_tweets, test_labels):
	""" Tests the current model on the current task.
	@param  model_name     The name of the model in the "models" folder.
	@param  classes_count  The amount of classes in the current task.
	@param  binary         Wether the task is of binary classification.
	@param  test_tweets    The tweets to be used during testing.
	@param  test_labels    The labels to be used during testing.
	"""
	def f1(y_true, y_pred):
		def recall(y_true, y_pred):
			true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
			possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
			recall = true_positives / (possible_positives + K.epsilon())
			return recall
		def precision(y_true, y_pred):
			true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
			predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
			precision = true_positives / (predicted_positives + K.epsilon())
			return precision
		precision = precision(y_true, y_pred)
		recall = recall(y_true, y_pred)
		return 2 * ((precision * recall) / (precision + recall + K.epsilon()))

	model = load_model("../models/" + model_name)
	results = model.predict(np.array(test_tweets))
	#model1A = load_model("../models/recognize_irony.h5")
	#model1B = load_model("../models/recognize_sarcasm.h5")
	
	confusion_matrix = np.zeros((len(classes), len(classes)), dtype=int)
	#results1 = model1A.predict(np.array(test_tweets))
	#resultsB = model1B.predict(np.array(test_tweets))
	for i in range(len(test_tweets)):
		if binary:
			if results[i].argmax() == 0:
				predicted_as = 0
			else:
				predicted_as = 1
		else:
			if results1[i][0] > 0.5:
				if resultsB[i][0] > 0.5:
					predicted_as = 2
				else:
					predicted_as = 1
			else:
				predicted_as = 0
			#predicted_as = results[i].argmax()
		confusion_matrix[test_labels[i], predicted_as] += 1
	print(confusion_matrix)
	
	return confusion_matrix


def common_metrics(confusion_matrix):
	precisions = []
	recalls = []
	trueps = []
	falsens = []
	falseps = []
	for i in range(len(classes)):
		truep = confusion_matrix[i, i]
		falsen = 0
		falsep = 0
		for j in range(len(classes)):
			if j != i:
				falsen += confusion_matrix[i, j]
				falsep += confusion_matrix[j, i]
		trueps.append(truep)
		falsens.append(falsen)
		falseps.append(falsep)
		if truep == 0:
			precision = 0
		else:
			precision = truep / (truep + falsep)
		recall = truep / (truep + falsen)
		precisions.append(precision)
		recalls.append(recall)
		if precision == 0 and recall == 0:
			f1 = 0
		else:
			f1 = 2 * precision * recall / (precision + recall)
		print("Class", i, "Precision:", precision, "Recall:", recall, "F1-score:", f1)
	micro_avg_precision = sum(trueps) / (sum(trueps) + sum(falseps))
	micro_avg_recall = sum(trueps) / (sum(trueps) + sum(falsens))
	print(micro_avg_precision, micro_avg_recall)
	if micro_avg_precision == 0 and micro_avg_recall == 0:
		micro_avg_f1score = 0
	else:
		micro_avg_f1score = 2 * micro_avg_precision * micro_avg_recall / (micro_avg_precision + micro_avg_recall)
	print("Micro-averaged F1-score:", micro_avg_f1score)
	macro_avg_precision = sum(precisions) / len(classes)
	macro_avg_recall = sum(recalls) / len(classes)
	if macro_avg_precision == 0 and macro_avg_recall == 0:
		macro_avg_f1score = 0
	else:
		macro_avg_f1score = 2 * macro_avg_precision * macro_avg_recall / (macro_avg_precision + macro_avg_recall)
	print("Macro-averaged F1-score:", macro_avg_f1score)
	accuracy = sum(trueps) / confusion_matrix.sum()
	print("Accuracy:", accuracy)
	return micro_avg_f1score, macro_avg_f1score, accuracy


if __name__ == "__main__":
	test_tweets = []
	test_labels = []
	for i in range(len(classes)):
		fp = codecs.open("../data/" + data_folder + "/" + classes[i] + "_cv.txt", "r", "utf-8")
		tweets = fp.readlines()
		add_tweets(tweets, test_tweets)
		fp.close()
		test_labels += [i] * len(tweets)
	confusion_matrix = test_model(model_name, len(classes), binary, test_tweets, test_labels)
	print()
	common_metrics(confusion_matrix)
