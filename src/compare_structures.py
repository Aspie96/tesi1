from cross_validation import get_cv_tweets
import numpy as np
from k_fold_cross_validation import create_folds, k_fold_cross_validation

structures = [0, 1]
k = 3

if __name__ == "__main__":
	confusion_matrices = []
	structures_metrics = []
	structures_metric = []
	tweets = get_cv_tweets()
	folded_tweets, folded_labels = create_folds(k, tweets)
	for structure_id in structures:
		results, metrics = k_fold_cross_validation(structure_id, k, folded_tweets, folded_labels)
		print()
		for i in range(k):
			print(results[i])
			print(metrics[i])
		result = np.sum(results, axis=0)
		metric = np.average(metrics, axis=0)
		confusion_matrices.append(result)
		structures_metrics.append(metrics)
		structures_metric.append(metric)
		print(result)
		print("Micro-averaged F1-score:", metric[0])
		print("Macro-averaged F1-score:", metric[1])
		print("Accuracy:", metric[2])

	print()
	for i in range(len(structures)):
		print("\nStructure", structures[i], ":")
		for metric in structures_metrics[i]:
			print(metric)
		print(confusion_matrices[i])
		print(structures_metric[i])
	print("\nSummary:")
	for i in range(len(structures)):
		print("Structure", structures[i], ":")
		print(structures_metric[i])
