# This script performs the k-fold cross validation.

k = 10	# Number of folds.
max_epochs = 120	# Maximum number of epochs during training.
stratified = True
structure_id = 0

from cross_validation import cross_validation, get_cv_tweets
from test_model import common_metrics
import numpy as np
from load import classes, model_name
from sklearn.utils import shuffle

def create_folds(k, tweets):
	folded_tweets = []
	folded_labels = []
	if stratified:
		for i in range(len(classes)):
			tweets[i] = shuffle(tweets[i])
			for j in range(k):
				if i == 0:
					folded_tweets.append([])
					folded_labels.append([])
				fold = tweets[i][round(j * len(tweets[i]) / k) : round((j + 1) * len(tweets[i]) / k)]
				folded_tweets[j] += fold
				folded_labels[j] += [i] * len(fold)
		for i in range(k):
			folded_tweets[i], folded_labels[i] = shuffle(folded_tweets[i], folded_labels[i])
	else:
		all_tweets = []
		all_labels = []
		for i in range(len(classes)):
			all_tweets += tweets[i]
			all_labels += [i] * len(tweets[i])
		all_tweets, all_labels = shuffle(all_tweets, all_labels)
		for i in range(k):
			folded_tweets[i] = all_tweets[round(i * len(all_tweets) / k) : round((i + 1) * len(all_tweets) / k)]
			folded_labels[i] = all_labels[round(i * len(all_tweets) / k) : round((i + 1) * len(all_tweets) / k)]
	return folded_tweets, folded_labels

def k_fold_cross_validation(structure_id, k, folded_tweets, folded_labels):
	results = []
	metrics = []
	for i in range(k):
		non_test_tweets = []
		non_test_labels = []
		for j in range(k):
			if j != i:
				non_test_tweets += folded_tweets[j]
				non_test_labels += folded_labels[j]
		train_tweets = non_test_tweets[:round(len(non_test_tweets) * 0.80)]
		train_labels = non_test_labels[:round(len(non_test_labels) * 0.80)]
		validation_tweets = non_test_tweets[round(len(non_test_tweets) * 0.80):]
		validation_labels = non_test_labels[round(len(non_test_labels) * 0.80):]
		test_tweets = folded_tweets[i]
		test_labels = folded_labels[i]
		result = cross_validation(structure_id, model_name, max_epochs, train_tweets, train_labels, validation_tweets, validation_labels, test_tweets, test_labels)
		results.append(result)
		metrics.append(common_metrics(result))
	return results, metrics

if __name__ == "__main__":
	tweets = get_cv_tweets()
	folded_tweets, folded_labels = create_folds(k, tweets)
	results, metrics = k_fold_cross_validation(structure_id, k, folded_tweets, folded_labels)
	print()
	for i in range(k):
		print(results[i])
		print(metrics[i])
	result = np.sum(results, axis=0)
	metric = np.average(metrics, axis=0)
	print(result)
	print("Micro-averaged F1-score:", metric[0])
	print("Macro-averaged F1-score:", metric[1])
	print("Accuracy:", metric[2])
